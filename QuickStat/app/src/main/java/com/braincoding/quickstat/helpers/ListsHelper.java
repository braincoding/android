package com.braincoding.quickstat.helpers;

import android.util.Log;

import com.braincoding.quickstat.entities.PageByDate;
import com.braincoding.quickstat.entities.Person;
import com.braincoding.quickstat.entities.Site;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Andrey Saprykin on 21.07.2016.
 */
public class ListsHelper {

    private static ListsHelper mInstance;
    private List<Person> mPersonsList;
    private List<Site> mSitesList;
    private List<PageByDate> mPageList;
    private List<PageByDate> mSteppedPageList;

    private int mTotalPage = 0;

    private ListsHelper() {
        mPersonsList = new ArrayList<>();
        mSitesList = new ArrayList<>();
        mPageList = new ArrayList<>();
        mSteppedPageList = new ArrayList<>();
    }

    public static ListsHelper getmInstance() {
        if (mInstance == null) {
            mInstance = new ListsHelper();
        }
        return mInstance;
    }

    /**
     * Возвращет список только имен существующих персон (для spinner)
     */
    public List<String> getPersonNameList() {
        List<String> personsName = new ArrayList<>();
        for (Person p : mPersonsList) {
            personsName.add(p.getName());
        }
        return personsName;
    }

    /**
     * Возвращет список только имен существующих сайтов (для spinner)
     */
    public List<String> getSiteNameList() {
        List<String> sitesName = new ArrayList<>();
        for (Site s : mSitesList) {
            sitesName.add(s.getSiteName());
        }
        return sitesName;
    }

    public List<Person> getPersonsList() {
        return mPersonsList;
    }

    public List<PageByDate> getSteppedPageList() {
        return mSteppedPageList;
    }

    public List<Site> getSitesList() {
        return mSitesList;
    }

    public void setPersonsList(List<Person> personsList) {
        this.mPersonsList = personsList;
    }

    public List<PageByDate> getPageList() {
        return mPageList;
    }

    public void setPageList(List<PageByDate> pageList) {
        if(!mPageList.isEmpty()){
            mPageList.clear();
        }
        mPageList.addAll(pageList);
        calcTotalPage();
    }

    /**Считаем общее количество страниц для данной персоны за указанный переод*/
    private void calcTotalPage(){
        mTotalPage = 0;
        for(PageByDate p : ListsHelper.getmInstance().getPageList()){
            mTotalPage += p.getPagesAmount();
        }
    }

    public void tempAddPagesList() {
        ArrayList<PageByDate> pageDate = new ArrayList<>();
        pageDate.add(new PageByDate("30.06.2016", 1));
        pageDate.add(new PageByDate("01.07.2016", 2));
        pageDate.add(new PageByDate("02.07.2016", 3));
        pageDate.add(new PageByDate("03.07.2016", 0));
        pageDate.add(new PageByDate("04.07.2016", 4));
        pageDate.add(new PageByDate("05.07.2016", 3));
        pageDate.add(new PageByDate("06.07.2016", 5));
        pageDate.add(new PageByDate("07.07.2016", 2));
        pageDate.add(new PageByDate("08.07.2016", 3));
        pageDate.add(new PageByDate("09.07.2016", 1));
        setPageList(pageDate);
    }

    /**Получаем наш PageList с учетом шага указанным в настройках, но не менее двух точек (первой и последней)*/
    public void setStepperPageList() {
        mSteppedPageList.clear();
        int step = SettingsHelper.getInstance().getChartStep();
        if (SettingsHelper.CHART_ONE_DAY_STEP != step) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(mPageList.get(0).getDate());
            cal.add(Calendar.DAY_OF_MONTH, step);
            Date stepDate = cal.getTime();
            mSteppedPageList.add(mPageList.get(0));
            for(PageByDate p: mPageList){
                if(p.getDate().equals(stepDate)){
                    mSteppedPageList.add(p);
                    cal.setTime(p.getDate());
                    cal.add(Calendar.DAY_OF_MONTH, step);
                    stepDate = cal.getTime();
                }
            }
            if(!mSteppedPageList.contains(mPageList.get(mPageList.size()-1))){
                mSteppedPageList.add(mPageList.get(mPageList.size()-1));
            }
        }else{
            mSteppedPageList.addAll(mPageList);
        }
    }

    /**Подсчитываем количество страниц*/
    public int getTotalPage() {
        return mTotalPage;
    }

    public void setSitesList(List<Site> sitesList) {
        mSitesList = sitesList;
    }

    /**Получении id сайта по имени*/
    public Integer getSiteId(String siteName){
        int siteId = 0;
        for (Site s: mSitesList){
            if(siteName.equals(s.getSiteName())){
                siteId = s.getSiteId();
                break;
            }
        }
        return siteId;
    }

    /**Получении id персоны по имени*/
    public Integer getPersonIdByName(String personName){
        int personId = 0;
        for(Person p : mPersonsList){
            if(personName.equals(p.getName())){
                personId = p.getId();
                break;
            }
        }
        return personId;
    }
}
