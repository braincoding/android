package com.braincoding.quickstat;
/**
 * Created by Andrey Saprykin
 */

import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.braincoding.quickstat.fragments.AboutFragment;
import com.braincoding.quickstat.fragments.AdminNameFragment;
import com.braincoding.quickstat.fragments.AdminPersonsFragment;
import com.braincoding.quickstat.fragments.AdminSitesFragment;
import com.braincoding.quickstat.fragments.CommonStatFragment;
import com.braincoding.quickstat.fragments.PersonStatFragment;
import com.braincoding.quickstat.fragments.SettingsFragment;
import com.braincoding.quickstat.helpers.NetConnectorHelper;
import com.braincoding.quickstat.helpers.SettingsHelper;

public class MainActivity extends AppCompatActivity {
    /**
     * Выдвижное меню
     */
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private String[] mFragmentTitles;
    private ListView mDrawPanel;

    /**
     * FragmentManager для управления семной фрагментов
     *
     */
    private static FragmentManager mFragmentManager;

    /**Swipe screens при помощи ViewPager*/
    /**
     * Количество страниц-экранов
     */
    private static final int NUM_PAGES = 4;
    /**
     * Pager widget, позволяет преключатся между экранами и анимировать их смену
     */
    private static ViewPager mPager;
    /**
     * PageAdapter обеспечивает ViewPage данными (pages)
     */
    private PagerAdapter mPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setDefaultData();
        setContentView(R.layout.activity_main);
        initView();
        if (savedInstanceState != null) {
            SettingsHelper.getInstance().setTipShow(savedInstanceState.getBoolean(SettingsHelper.STATE_TIP));
            SettingsHelper.getInstance().setChartStepByPosition(savedInstanceState.getInt(SettingsHelper.STATE_STEP));
        }
    }

    private void setDefaultData() {
        if(isNetworkAvailable()){
            SettingsHelper.getInstance().setNetAvailable(true);

            NetConnectorHelper netConnectorHelper = new NetConnectorHelper();
            netConnectorHelper.setSiteUrl();
            netConnectorHelper.setPersonsUrl(1);
            netConnectorHelper.setPagesUrl(3, 3, "01.07.2016", "08.07.2016");
        }else{
            SettingsHelper.getInstance().showAppTips(this, getResources().getString(R.string.net_not_available));
        }

    }

    /**
     * Получение View компонета по Id
     */
    private <T> T findComponentById(int id) {
        return (T) this.findViewById(id);
    }

    /**
     * Инициализация переменных
     */
    private void initVar() {
        mFragmentManager = getSupportFragmentManager();
        if (SettingsHelper.getInstance().isAdminChecked()) {
            mFragmentTitles = getResources().getStringArray(R.array.drawer_admin_menu_titles);
        } else {
            mFragmentTitles = getResources().getStringArray(R.array.drawer_menu_titles);
        }
    }

    /**
     * Инициализация View компонентов
     */
    private void initView() {
        initVar();

         /*ViewPager*/
        mPager = findComponentById(R.id.pager);
        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);

        /*Navigation Drawable*/
        mDrawerLayout = findComponentById(R.id.drawer_menu_layout);
        mDrawPanel = findComponentById(R.id.drawer_list_view);
        mDrawPanel.setAdapter(new ArrayAdapter<String>(this, R.layout.drawer_list_item, mFragmentTitles));

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.empty, R.string.empty) {
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
            }

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };

        mDrawPanel.setOnItemClickListener(new DrawerItemClickListener());
        mDrawerLayout.addDrawerListener(mDrawerToggle);

        this.getSupportActionBar().setTitle("");
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    /**Перерисовка компонентов, если поменялось на меню админки*/
    @Override
    protected void onResume() {
        super.onResume();
        mPagerAdapter.notifyDataSetChanged();
    }

    /**Создаем меню в Action Bar*/
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    /**Обработка нажатия на опции меню  Action Bar-а*/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        switch (item.getItemId()) {
            case R.id.menu_settings:
                setCurrentScreen(2);
                return true;
           /* case R.id.menu_admin:
                SettingsHelper.getInstance().setAdminChecked(!SettingsHelper.getInstance().isAdminChecked());
                notifyViewAdapter();
                return true;*/
            default:
                return false;
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }


    /**
     * Слушатель нажатия на элемент выдвежного меню
     */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            setCurrentScreen(position);
        }
    }

    /**
     * Смена экрана по позиции
     *
     * @param position Позиция фрагмента(экрана)
     */
    private void setCurrentScreen(int position) {
        mPager.setCurrentItem(position);
        mDrawPanel.setItemChecked(position, true);
        mDrawPanel.setSelection(position);
        mDrawerLayout.closeDrawer(mDrawPanel);
    }

    /**
     * Возврат фрагмента по позиции
     *
     * @param position Позиция экрана
     */
    private Fragment getScreenFragment(int position) {
        if (SettingsHelper.getInstance().isAdminChecked()) {
            switch (position) {
                case 1:
                    return new AdminSitesFragment();
                case 2:
                    return new AdminPersonsFragment();
                default:
                    return new AdminNameFragment();
            }
        } else {
            switch (position) {
                case 1:
                    return new PersonStatFragment();
                case 2:
                    return new SettingsFragment();
                case 3:
                    return new AboutFragment();
                default:
                    return new CommonStatFragment();
            }
        }
    }

    /**Метод нажатия кнопки Back, если вернулись к первому фрагменту - то выходим, если нет отображаем предидущий фрагмент*/
    @Override
    public void onBackPressed() {
        if (mPager.getCurrentItem() == 0) {
            // If the user is currently looking at the first step, allow the system to handle the
            // Back button. This calls finish() on this activity and pops the back stack.
            super.onBackPressed();
        } else {
            // Otherwise, select the previous step.
            mPager.setCurrentItem(mPager.getCurrentItem() - 1);
        }
    }


    /**
     * Простой pager adapter предоставляет данные (экраны) в очереди
     */
    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {

        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public Fragment getItem(int position) {
            return getScreenFragment(position);
        }

        @Override
        public int getCount() {
            if (mFragmentTitles != null && mFragmentTitles.length != 0) {
                return mFragmentTitles.length;
            } else {
                return NUM_PAGES;
            }
        }

    }

    /**Статический метод обновления данных для адаптера ViewPager*/
    public static void notifyViewAdapter(){
        if (mPager != null){
            mPager.getAdapter().notifyDataSetChanged();
        }
    }

    /**Метод проверки подключения к сети*/
    private boolean isNetworkAvailable(){
        ConnectivityManager connectivityManager = (ConnectivityManager) this.getSystemService(CONNECTIVITY_SERVICE);
        return (connectivityManager.getActiveNetworkInfo() !=null && connectivityManager.getActiveNetworkInfo().isConnected());
    }
}
