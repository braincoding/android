package com.braincoding.quickstat.entities;

import android.graphics.Color;

import java.util.Random;

/**
 * Created by Andrey Saprykin on 11.07.2016.
 * Рейтинг персоны
 */
public class Person {
    private int mId;
    private String mName;
    private int mRank;
    private int mColor;
    private Random mRandom = new Random();

    public Person() {
    }

    public Person(int id, String name, int rank) {
        setId(id);
        mName = name;
        setRank(rank);
        mColor = genColor();
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        if(id >= 0){
            mId = id;
        }else{
            mId = 0;
        }
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public int getRank() {
        return mRank;
    }

    public void setRank(int rank) {
        if (rank >= 0) {
            this.mRank = rank;
        } else {
            this.mRank = 0;
        }
    }

    private Integer genColor(){
        return Color.rgb(mRandom.nextInt(255),mRandom.nextInt(255),mRandom.nextInt(255));
    }

    public int getColor() {
        return mColor;
    }
}
