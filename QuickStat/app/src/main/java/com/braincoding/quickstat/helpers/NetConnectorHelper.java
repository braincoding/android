package com.braincoding.quickstat.helpers;

import android.os.AsyncTask;

import com.braincoding.quickstat.MainActivity;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Andrey Saprykin on 23.07.2016.
 */
public class NetConnectorHelper {

    private final String PERSON_TYPE = "common?";
    private final String PAGE_TYPE = "daily?";

    private String SITES_URL = "http://quickstat.cf/rest_php/v1/sites";
    private String PERSONS_URL = "http://quickstat.cf/rest_php/v1/stat/common?site_id=%d";
    private String PAGES_URL = "http://quickstat.cf/rest_php/v1/stat/daily?site_id=%d&person_id=%d&first_date=%s&last_date=%s";

    JsonHelper helper = new JsonHelper();

    public NetConnectorHelper() {

    }

    public void setPagesUrl(int siteId, int personId, String beginDate, String endDate) {
        ListsHelper.getmInstance().getPageList().clear();
        DO(String.format(PAGES_URL, siteId, personId, beginDate, endDate));
    }

    public void setSiteUrl(){
        DO(SITES_URL);
    }

    public void setPersonsUrl(int id){
        DO(String.format(PERSONS_URL, id));
    }

    public void DO(String urlAddress) {
        new UpdateData().execute(urlAddress);
    }

    private class UpdateData extends AsyncTask<String, Void, Void> {
        @Override
        protected Void doInBackground(String... params) {
            try {
                getData(params[0]);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            MainActivity.notifyViewAdapter();
        }

        private void getData(String urlAddress) throws IOException {
            URL url = new URL(urlAddress);
            HttpURLConnection c = (HttpURLConnection) url.openConnection();
            c.setRequestMethod("GET");
            c.connect();
            InputStream ins = c.getInputStream();
            if(urlAddress.equals(SITES_URL)){
                ListsHelper.getmInstance().setSitesList(helper.getSitesList(ins));
            }else if (urlAddress.contains(PERSON_TYPE)) {
                ListsHelper.getmInstance().setPersonsList(helper.getPersonsRanks(ins));
            }else if(urlAddress.contains(PAGE_TYPE)) {
                ListsHelper.getmInstance().setPageList(helper.getPagesByDates(ins));
            }
            ins.close();
            c.disconnect();
        }
    }
}
