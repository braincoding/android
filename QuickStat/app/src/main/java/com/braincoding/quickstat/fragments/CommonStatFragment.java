package com.braincoding.quickstat.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import com.braincoding.quickstat.MainActivity;
import com.braincoding.quickstat.R;
import com.braincoding.quickstat.adapters.CustomAdapter;
import com.braincoding.quickstat.helpers.ListsHelper;
import com.braincoding.quickstat.helpers.NetConnectorHelper;
import com.braincoding.quickstat.helpers.SettingsHelper;

import java.util.ArrayList;
import java.util.List;

import lecho.lib.hellocharts.model.PieChartData;
import lecho.lib.hellocharts.model.SliceValue;
import lecho.lib.hellocharts.view.PieChartView;

/**
 * Created by Sergey Kudishov
 * Improved by Andrey Saprykin
 * */

public class CommonStatFragment extends Fragment {
    private ListView listView;
    public CustomAdapter ca;
    private Spinner spinner;
    PieChartView pcv;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
//        createUsers();
        View v = inflater.inflate(R.layout.fragment_common_stat, container, false);
        initView(v);

        setChartData(pcv);

        return v;
    }

    /**
     * Получение View компонета по Id
     */
    private <T> T findComponentById(View customView, int id) {
        return (T) customView.findViewById(id);
    }

    /**
     * Инициализация всех View компонентов нашего фрагмента
     */
    private void initView(View customView) {
        pcv = findComponentById(customView, R.id.viewPie);
        spinner = findComponentById(customView, R.id.spinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), R.layout.spinner_item, ListsHelper.getmInstance().getSiteNameList());
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        SpinnerItemSelectedListener spinnerListenner = new SpinnerItemSelectedListener();
        spinner.setOnTouchListener(spinnerListenner);
        spinner.setOnItemSelectedListener(spinnerListenner);
        spinner.setSelection(1);
        listView = findComponentById(customView, R.id.listView);
        ca = new CustomAdapter(ListsHelper.getmInstance().getPersonsList(), getActivity());
        //assert listView != null;
        listView.setAdapter(ca);
    }


    private void setChartData(PieChartView pcv) {
        PieChartData data = new PieChartData();
        List<SliceValue> values = new ArrayList<>();
        for (int i = 0; i < ListsHelper.getmInstance().getPersonsList().size(); i++) {
            values.add(0, new SliceValue(ListsHelper.getmInstance().getPersonsList().get(i).getRank(), ListsHelper.getmInstance().getPersonsList().get(i).getColor()));
        }
        data.setValues(values);
        pcv.setPieChartData(data);
    }

    private void updateData(String siteName) {
        SettingsHelper.getInstance().showAppTips(getActivity(), siteName);
        NetConnectorHelper net = new NetConnectorHelper();
        net.setPersonsUrl(ListsHelper.getmInstance().getSiteId(siteName));
        ca.notifyDataSetChanged();
        MainActivity.notifyViewAdapter();
    }

    /**Слушатель для выпадающего списка*/
    private class SpinnerItemSelectedListener implements AdapterView.OnItemSelectedListener, View.OnTouchListener{
        private boolean isUsterToch = false;

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            isUsterToch = true;
            return false;
        }

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if(isUsterToch){
                updateData(spinner.getItemAtPosition(position).toString());
                isUsterToch = false;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }

    }



}