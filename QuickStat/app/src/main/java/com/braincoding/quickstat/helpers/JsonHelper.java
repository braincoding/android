package com.braincoding.quickstat.helpers;

import android.util.JsonReader;
import android.util.Log;

import com.braincoding.quickstat.entities.PageByDate;
import com.braincoding.quickstat.entities.Person;
import com.braincoding.quickstat.entities.Site;
import com.braincoding.quickstat.fragments.AboutFragment;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrey Saprykin on 15.07.2016.
 */
public class JsonHelper {

    public static final String JSON_TAG = JsonHelper.class.getSimpleName();


    /**
     * Получить персону с рейтингом по сайту
     */
    private Person readPersonRank(JsonReader reader) throws IOException {
        int id = -1;
        String personName = "";
        int personRank = -1;

        reader.beginObject();
        while (reader.hasNext()) {
            String jsonVar = reader.nextName();
            if (jsonVar.equals("id")) {
                id = reader.nextInt();
            } else if (jsonVar.equals("name")) {
                personName = reader.nextString();
            } else if (jsonVar.equals("rank")) {
                personRank = reader.nextInt();
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();

        return new Person(id, personName, personRank);
    }

    /**
     * Получить общую статистику всех персон по сайту
     */
    private List<Person> readPersonsRankList(JsonReader reader) throws IOException {
        List<Person> persons = new ArrayList<>();
        reader.beginArray();
        while (reader.hasNext()) {
            persons.add(readPersonRank(reader));
        }
        reader.endArray();
        return persons;
    }

    /**
     * Возвращает статистику всех персон по сайту
     */
    public List<Person> getPersonsRanks(InputStream ins) throws IOException {
        JsonReader jsonReader = new JsonReader(new InputStreamReader(ins, "UTF-8"));
        try {
            return readPersonsRankList(jsonReader);
        } catch (IOException e) {
            Log.e(JSON_TAG, e.getMessage());
            return null;
        } finally {
            jsonReader.close();
        }
    }


    /**
     * Прочитать количество страниц и дату по указанной персоне на определенном сайте
     */
    private PageByDate readPagesByDate(JsonReader reader) throws IOException {
        String pageDate = "";
        int pageAmount = 0;

            reader.beginObject();
            while (reader.hasNext()) {
                String jsonVar = reader.nextName();
                if (jsonVar.equals("date")) {
                    pageDate = reader.nextString();
                } else if (jsonVar.equals("pages")) {
                    pageAmount = reader.nextInt();
                } else {
                    reader.skipValue();
                }
            }
            reader.endObject();
            return new PageByDate(pageDate, pageAmount);

    }

    /**
     * Прочитать список ежидневной статистики по сайту на конкретную персону
     */
    private List<PageByDate> readPagesAmountList(JsonReader reader) throws IOException {
        List<PageByDate> pages = new ArrayList<>();
        reader.beginArray();
        while (reader.hasNext()) {
            pages.add(readPagesByDate(reader));
        }
        reader.endArray();
        return pages;
    }

    /**Промежуточный метод поскольку массив содержится в объекте*/
    private List<PageByDate> readFirstPageObject(JsonReader reader) throws IOException{
        List<PageByDate> pagesList = new ArrayList<>();
        reader.beginObject();
        while (reader.hasNext()){
            String jsonVar = reader.nextName();
            if (jsonVar.equals("pagesByDays")){
                pagesList = readPagesAmountList(reader);
            }else {
                reader.skipValue();
            }
        }
        reader.endObject();
        return pagesList;
    }

    /**
     * Возвращает ежидневную статистику сайта по персоне
     */
    public List<PageByDate> getPagesByDates(InputStream ins) throws IOException {
        JsonReader jsonReader = new JsonReader(new InputStreamReader(ins, "UTF-8"));
        try {
            return readFirstPageObject(jsonReader);
        } catch (IOException e) {
            Log.e("", e.getMessage());
            return null;
        } finally {
            jsonReader.close();
        }
    }

    /**
     * Получить сайт
     */
    private Site readSite(JsonReader reader) {
        int siteId = 0;
        String siteName = Site.WRONG_SITE_NAME;
        try {
            reader.beginObject();
            while (reader.hasNext()) {
                String jsonVar = reader.nextName();
                if (jsonVar.equals("id")) {
                    siteId = reader.nextInt();
                } else if (jsonVar.equals("name")) {
                    siteName = reader.nextString();
                } else {
                    reader.skipValue();
                }
            }
            reader.endObject();
        } catch (IOException e) {
            Log.e(JSON_TAG, e.getMessage());
        }
        return new Site(siteId, siteName);
    }

    /**
     * Прочитать список сайтов
     */
    private List<Site> readSitesList(JsonReader reader) {
        List<Site> sites = new ArrayList<>();
        try {
            reader.beginArray();
            while (reader.hasNext()) {
                Site tempSite = readSite(reader);
                if (!tempSite.getSiteName().equals(Site.WRONG_SITE_NAME)) {
                    sites.add(tempSite);
                }
            }
            reader.endArray();
        } catch (IOException e) {
            Log.e(JSON_TAG, e.getMessage());
        }
        return sites;
    }

    /**
     * Возвращает список сайтов
     */
    public List<Site> getSitesList(InputStream ins) throws IOException {
        JsonReader jsonReader = new JsonReader(new InputStreamReader(ins, "UTF-8"));
        try {
            return readSitesList(jsonReader);
        } catch (Exception e) {
            Log.e("", e.getMessage());
            return null;
        } finally {
            jsonReader.close();
        }
    }
}
