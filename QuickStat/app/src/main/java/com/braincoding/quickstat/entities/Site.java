package com.braincoding.quickstat.entities;

/**
 * Created by Andrey Saprykin on 20.07.2016.
 * Определенный сайт на котором производится поиск
 */
public class Site {
    public static String WRONG_SITE_NAME = "Unknown site";

    private int mSiteId;
    private String mSiteName;

    public Site() {
    }

    public Site(int siteId, String siteName) {
        mSiteId = siteId;
        mSiteName = siteName;
    }

    public int getSiteId() {
        return mSiteId;
    }

    public void setSiteId(int siteId) {
        mSiteId = siteId;
    }

    public String getSiteName() {
        return mSiteName;
    }

    public void setSiteName(String siteName) {
        if (!siteName.equals("") && siteName != null) {
            mSiteName = siteName;
        } else {
            mSiteName = WRONG_SITE_NAME;
        }
    }
}
