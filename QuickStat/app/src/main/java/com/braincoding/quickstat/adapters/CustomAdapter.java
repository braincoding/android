package com.braincoding.quickstat.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.braincoding.quickstat.R;
import com.braincoding.quickstat.entities.Person;
import java.util.List;


public class CustomAdapter extends BaseAdapter {
    private List<Person> userAdapterArrayList;
    private Context context;
    private TextView tvNname;
    private TextView tvRating;
    private ImageView ivColor;

    public CustomAdapter(List userAdapterArrayList, Context context) {
        this.userAdapterArrayList = userAdapterArrayList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return userAdapterArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return userAdapterArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Person p = userAdapterArrayList.get(position);

        View usersView = convertView;
        if (usersView == null) {
            usersView = LayoutInflater.from(context).inflate(R.layout.person_list_view, null);
//            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//            arg1 = inflater.inflate(R.layout.view_list, arg2,false);
        }

        tvNname = (TextView) usersView.findViewById(R.id.name);
        tvNname.setText(p.getName());
        tvRating = (TextView) usersView.findViewById(R.id.textRating);
        int i = p.getRank();
        String str = Integer.toString(i);
        tvRating.setText(str);

        ivColor = (ImageView) usersView.findViewById(R.id.statusImage);
        ivColor.setBackgroundColor(p.getColor());

        return usersView;
    }
}
