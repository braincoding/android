package com.braincoding.quickstat.helpers;

import android.content.Context;
import android.widget.Toast;

import com.braincoding.quickstat.MainActivity;

/**
 * Created by Andrey Saprykin
 */
public class SettingsHelper {
    private static SettingsHelper mInstance;

    public static final int CHART_ONE_DAY_STEP = 1;
    public static final int CHART_THREE_DAY_STEP = 3;
    public static final int CHART_FIVE_DAY_STEP = 5;
    public static final int CHART_ONE_WEEK_STEP = 7;

    public static final String STATE_TIP = "TipShow";
    public static final String STATE_STEP = "CartStep";

    private boolean mAdminChecked;
    private boolean mTipShow;
    private boolean mIsStepChange;
    private boolean mIsNetAvailable;
    private int mChartStep;

    public static SettingsHelper getInstance() {
        if (mInstance == null) {
            mInstance = new SettingsHelper();
        }
        return mInstance;
    }

    private SettingsHelper() {
        mAdminChecked = false;
        //TODO: Shared Preference
        mTipShow = true;
        mChartStep = CHART_ONE_DAY_STEP;
    }

    public boolean isAdminChecked() {
        return mAdminChecked;
    }

    public void setAdminChecked(boolean adminChecked) {
        mAdminChecked = adminChecked;
        MainActivity.notifyViewAdapter();
    }

    public boolean isTipShow() {
        return mTipShow;
    }

    public void setTipShow(boolean tipShow) {
        mTipShow = tipShow;
    }

    public int getChartStep() {
        return mChartStep;
    }

    private void setChartStep(int chartStep) {
        mChartStep = chartStep;
    }

    /**Вывод подсказок на экран при помощи Toast
     * @param context контекст приложения
     * @param message текст сообщения-подсказки*/
    public void showAppTips(Context context, String message){
        if(isTipShow())
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    /**Установка "шага" графика по выбранной позиции chartStepSpinner
     * @param position позиция элемента в списке chartStepSpinner*/
    public void setChartStepByPosition(int position){
        switch (position){
            case 0:
                setChartStep(CHART_ONE_DAY_STEP);
                break;
            case 1:
                setChartStep(CHART_THREE_DAY_STEP);
                break;
            case 2:
                setChartStep(CHART_FIVE_DAY_STEP);
                break;
            case 3:
                setChartStep(CHART_ONE_WEEK_STEP);
                break;
            default:
                setChartStep(CHART_ONE_DAY_STEP);
                break;
        }
    }

    /**Возвращает позицию по шагу графика.*/
    public Integer getCartSpinnerPosition(){
        switch (mChartStep){
            case CHART_THREE_DAY_STEP:
                return 1;
            case CHART_FIVE_DAY_STEP:
                return 2;
            case CHART_ONE_WEEK_STEP:
                return 3;
            default:
                return 0;
        }
    }

    /**Изменился ли шаг графика*/
    public boolean isStepChange() {
        return mIsStepChange;
    }

    /**Установить флаг смены шага*/
    public void setStepChange(boolean stepChange) {
        mIsStepChange = stepChange;
    }

    public boolean isNetAvailable() {
        return mIsNetAvailable;
    }

    public void setNetAvailable(boolean netAvailable) {
        mIsNetAvailable = netAvailable;
    }
}
