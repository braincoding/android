package com.braincoding.quickstat.entities;

import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Andrey Saprykin on 19.07.2016.
 * Количество новых страниц на сайте на определенную дату по персоне
 */
public class PageByDate {
    /**TAG для логирования исключения*/
    public static  final String PAGE_TAG = PageByDate.class.getSimpleName();

    /**Дата*/
    private Date mDate;
    /**Количество страниц на которых упоминаеться персона на заданном сайте*/
    private int mPagesAmount;
    /**Форматтер даты для текущей локали*/
    private SimpleDateFormat mDateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());

    public PageByDate() {
    }

    public PageByDate(Date date, int pagesAmount) {
        mDate = date;
        mPagesAmount = pagesAmount;
    }

    public PageByDate(String date, int pagesAmount) {
        try {
            mDate = mDateFormat.parse(date);
        } catch (ParseException e) {
            Log.e(PAGE_TAG, e.getMessage());
            mDate = Calendar.getInstance().getTime();
        }
        mPagesAmount = pagesAmount;
    }

    public Date getDate() {
        return mDate;
    }

    public String getDateInString(){
        return mDateFormat.format(mDate);
    }

    public void setDate(Date date) {
        mDate = date;
    }

    public void setDate(String date){
        try {
            mDate = mDateFormat.parse(date);
        } catch (ParseException e) {
            Log.e(PAGE_TAG, e.getMessage());
            mDate = Calendar.getInstance().getTime();
        }
    }

    public int getPagesAmount() {
        return mPagesAmount;
    }

    public void setPagesAmount(int pagesAmount) {
        mPagesAmount = pagesAmount;
    }

    @Override
    public String toString() {
        return "Date ="+mDate.toString()+" Amount ="+mPagesAmount;
    }
}
