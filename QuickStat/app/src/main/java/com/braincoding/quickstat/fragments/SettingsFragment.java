package com.braincoding.quickstat.fragments;
/**Created by Andrey Saprykin*/

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.Spinner;

import com.braincoding.quickstat.MainActivity;
import com.braincoding.quickstat.R;
import com.braincoding.quickstat.helpers.ListsHelper;
import com.braincoding.quickstat.helpers.SettingsHelper;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends Fragment {
    private Spinner mChartStepSpinner;
    private ArrayAdapter<CharSequence> mSpinnerAdapter;
    private CheckBox mShowTipsCb;
    private Context mContext;

    public SettingsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        mSpinnerAdapter = ArrayAdapter.createFromResource(getActivity(), R.array.chart_steps_txt, R.layout.spinner_item);
        mSpinnerAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View customView = inflater.inflate(R.layout.fragment_settings, container, false);
        initView(customView);
        return customView;
    }

    /**Получение View компонета по Id
     * @param v - представление макета
     * @param id - идентификатор компонента*/
    private <T> T findComponentById(View v, int id){
        return (T) v.findViewById(id);
    }

    /**Инициализация View компонентов*/
    private void initView(View v){
        mShowTipsCb = findComponentById(v, R.id.cb_show_tips);
        mShowTipsCb.setChecked(SettingsHelper.getInstance().isTipShow());
        mShowTipsCb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SettingsHelper.getInstance().setTipShow(mShowTipsCb.isChecked());
            }
        });

        mChartStepSpinner = findComponentById(v, R.id.spin_chart_step);
        mChartStepSpinner.setAdapter(mSpinnerAdapter);
        mChartStepSpinner.setSelection(SettingsHelper.getInstance().getCartSpinnerPosition());
        ChartSpinnerItemSelected chartSpinnerListener = new ChartSpinnerItemSelected();
        mChartStepSpinner.setOnTouchListener(chartSpinnerListener);
        mChartStepSpinner.setOnItemSelectedListener(chartSpinnerListener);
    }

    private class ChartSpinnerItemSelected implements AdapterView.OnItemSelectedListener, View.OnTouchListener {
        private boolean isUserTouch = false;

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            isUserTouch = true;
            return false;
        }

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if(isUserTouch){
                SettingsHelper.getInstance().setChartStepByPosition(position);
                //Код изменения для фрагментов
                //ListsHelper.getmInstance().setStepperPageList();
                MainActivity.notifyViewAdapter();
                isUserTouch = false;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
            SettingsHelper.getInstance().showAppTips(mContext,getResources().getString(R.string.tip_nothing_selected));
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(SettingsHelper.STATE_TIP, SettingsHelper.getInstance().isTipShow());
        outState.putInt(SettingsHelper.STATE_STEP,SettingsHelper.getInstance().getChartStep());
    }

}
