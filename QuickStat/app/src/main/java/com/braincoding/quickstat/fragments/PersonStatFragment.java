package com.braincoding.quickstat.fragments;


import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;

import com.braincoding.quickstat.R;
import com.braincoding.quickstat.helpers.ListsHelper;
import com.braincoding.quickstat.helpers.NetConnectorHelper;
import com.braincoding.quickstat.helpers.SettingsHelper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import lecho.lib.hellocharts.model.Line;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.PointValue;
import lecho.lib.hellocharts.view.LineChartView;

/**
 * Created by Andrey Saprykin
 */
public class PersonStatFragment extends Fragment {
    /**
     * Tag для логрирования
     */
    private String PSF_TAG = PersonStatFragment.class.getSimpleName();
    private final String mNone = "none";

    private Button mStartDate;
    private Button mEndDate;
    private FloatingActionButton mUpdateData;

    private TextView textStartDate;
    private TextView textEndDate;
    private TextView textViewRate;

    private Calendar mCalendar;
    private SimpleDateFormat mDateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());

    private LineChartView mLineChart;

    private Spinner mPersonSpinner;
    private Spinner mSiteSpinner;
    private String mSiteName = mNone;
    private String mPersonName = mNone;


    private ArrayAdapter<String> mPersonAdapter;
    private ArrayAdapter<String> mSiteAdapter;

    private Calendar cal = Calendar.getInstance();
    private int mCurrentDay = cal.get(Calendar.DAY_OF_MONTH);
    private int mCurrentMonth = cal.get(Calendar.MONTH);
    private int mCurrentYear = cal.get(Calendar.YEAR);

    private int mTotalPage = 0;

    public PersonStatFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setDefaultFragmentData();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View fragmentView = inflater.inflate(R.layout.fragment_person_stat, container, false);
        initView(fragmentView);
        return fragmentView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void onClickStartDate(View view) {
        new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                textStartDate.setText(getFormattedDate(year, monthOfYear, dayOfMonth));
            }
        }, mCurrentYear, mCurrentMonth, 01)
                .show();
    }

    public void onClickEndDate(View view) {
        new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                textEndDate.setText(getFormattedDate(year, monthOfYear, dayOfMonth));
            }
        }, mCurrentYear, mCurrentMonth, mCurrentDay)
                .show();
    }

    /**
     * Получение View компонета по Id
     */
    private <T> T findComponentById(View customView, int id) {
        return (T) customView.findViewById(id);
    }

    /**
     * Инициализация всех View компонентов нашего фрагмента
     */
    private void initView(View customView) {
        mCalendar = Calendar.getInstance();

        textEndDate = findComponentById(customView, R.id.textEndDate);
        textStartDate = findComponentById(customView, R.id.textStartDate);

        mStartDate = findComponentById(customView, R.id.buttonStartDate);
        mStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickStartDate(v);
            }
        });
        mEndDate = findComponentById(customView, R.id.buttonEndDate);
        mEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View vv) {
                onClickEndDate(vv);
            }
        });

        mSiteSpinner = findComponentById(customView, R.id.spinnerSite);
        mPersonSpinner = findComponentById(customView, R.id.spinnerPerson);

        mSiteAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, ListsHelper.getmInstance().getSiteNameList());
        mPersonAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, ListsHelper.getmInstance().getPersonNameList());

        mSiteSpinner.setAdapter(mSiteAdapter);
        mPersonSpinner.setAdapter(mPersonAdapter);

        SpinnerItemListener sil = new SpinnerItemListener();
        mSiteSpinner.setOnTouchListener(sil);
        mSiteSpinner.setOnItemSelectedListener(sil);
        mSiteSpinner.setSelection(2);
        mPersonSpinner.setOnTouchListener(sil);
        mPersonSpinner.setOnItemSelectedListener(sil);
        mPersonSpinner.setSelection(2);

        mUpdateData = findComponentById(customView, R.id.update_data_fab);
        mUpdateData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateData();
            }
        });

        mLineChart = findComponentById(customView, R.id.line_chart);
        setChartData(mLineChart);

        textViewRate = findComponentById(customView, R.id.textViewRate);
        textViewRate.setText(String.format(getResources().getString(R.string.total_page_amount), mTotalPage));
    }


    /**
     * Устанавливаем данные в линейный график
     *
     * @param lcView - представление графика (view)
     */
    private void setChartData(LineChartView lcView) {
        LineChartData data = new LineChartData();
        List<PointValue> values = new ArrayList<PointValue>();

        for (int i = 0; i < ListsHelper.getmInstance().getSteppedPageList().size(); i++) {
            values.add(new PointValue(i, ListsHelper.getmInstance().getSteppedPageList().get(i).getPagesAmount()));
        }


        int color = getResources().getColor(R.color.colorPrimaryDark);//deprecated метод выбрат потому что minVer=15, а не 23 /
        Line line = new Line(values).setColor(color).setCubic(false);
        List<Line> lines = new ArrayList<Line>();
        lines.add(line);

        data.setLines(lines);
        lcView.setLineChartData(data);
    }

    /**
     * Устанавливаем начальные значения для этого фрагмента
     */
    private void setDefaultFragmentData() {
        ListsHelper.getmInstance().setStepperPageList();
        mTotalPage = ListsHelper.getmInstance().getTotalPage();
        if (!ListsHelper.getmInstance().getSiteNameList().isEmpty() && !ListsHelper.getmInstance().getPersonNameList().isEmpty()) {
            mSiteName = ListsHelper.getmInstance().getSiteNameList().get(0);
            mPersonName = ListsHelper.getmInstance().getPersonNameList().get(0);
        }
    }

    /**
     * Возвращает дату в виде отформатированной сторки
     *
     * @param year  - год
     * @param month - месяц
     * @param day   - день
     */
    private String getFormattedDate(int year, int month, int day) {
        mCalendar.set(Calendar.DAY_OF_MONTH, day);
        mCalendar.set(Calendar.MONTH, month);
        mCalendar.set(Calendar.YEAR, year);
        return mDateFormat.format(mCalendar.getTime());
    }

    /**
     * Обновление данных по условиям выбранным пользователем
     */
    private void updateData() {
        if (SettingsHelper.getInstance().isNetAvailable()) {
            String startDate = textStartDate.getText().toString();
            String endDate = textEndDate.getText().toString();
            //если один из параметров не установлен то выводим сообщение об ошибке
            if (verifyStringParametr(mSiteName, mPersonName, startDate, endDate)) {
                SettingsHelper.getInstance().showAppTips(getContext(), getString(R.string.error_input_parametrs));
            } else {
                int siteId = ListsHelper.getmInstance().getSiteId(mSiteName);
                int personId = ListsHelper.getmInstance().getPersonIdByName(mPersonName);
                NetConnectorHelper netHelper = new NetConnectorHelper();
                netHelper.setPagesUrl(siteId, personId, startDate, endDate);
            }
        } else {
            SettingsHelper.getInstance().showAppTips(getContext(), getResources().getString(R.string.net_not_available));
        }
    }

    /**
     * вспомогательный метод проверки строковых параметров на пустоту или "none"
     */
    private boolean verifyStringParametr(String... parametrs) {
        boolean isWrong = false;
        for (String s : parametrs) {
            if (s.equals(null) || s.equals("") || s.equals(mNone)) {
                isWrong = true;
                break;
            }
        }
        return isWrong;
    }

    /**Слушатель для выпадающих списков*/
    private class SpinnerItemListener implements AdapterView.OnItemSelectedListener, View.OnTouchListener {
        boolean isUserTouch = false;
        int selectedViewId;

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            isUserTouch = true;
            selectedViewId = v.getId();
            return false;
        }

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if (isUserTouch) {
                switch (selectedViewId) {
                    case R.id.spinnerSite:
                        mSiteName = (String) mSiteSpinner.getItemAtPosition(position);
                        break;
                    case R.id.spinnerPerson:
                        mPersonName = (String) mPersonSpinner.getItemAtPosition(position);
                        break;
                }
                isUserTouch = false;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
            SettingsHelper.getInstance().showAppTips(getContext(), getResources().getString(R.string.tip_nothing_selected));
        }
    }
}
